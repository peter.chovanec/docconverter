﻿using DocConverter.AppLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocConverter.UnitTests.Services.Fixtures
{
    internal class DocumentsFixture
    {
        public static List<Document> GetTestDocs() =>
            new()
            {
                new Document()
                    {
                        Title = "Test title 1",
                        Text = "This is the test of common text"
                    },
                new Document()
                    {
                        Title = "Test title 2",
                        Text = "This is the test of numbers {123456789}"
                    },
                new Document()
                    {
                        Title = "Test title 3",
                        Text = "This is the test of special {!@#$%^*} characters"
                    }
            };
    }
}
