﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocConverter.UnitTests.Services.Fixtures
{
    internal class XmlStringsFixture
    {
        public static List<string> GetTestDocs() =>
            new()
            {
                "<document><title>Test title 1</title><text>This is the test of common text</text></document>",
                "<document><title>Test title 2</title><text>This is the test of numbers {123456789}</text></document>",
                "<document><title>Test title 3</title><text>This is the test of special {!@#$%^*} characters</text></document>"
            };
    }
}
