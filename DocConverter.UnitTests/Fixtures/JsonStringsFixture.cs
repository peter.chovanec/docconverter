﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocConverter.UnitTests.Services.Fixtures
{
    internal class JsonStringsFixture
    {
        public static List<string> GetTestDocs() =>
            new()
            {
                "{\"Title\":\"Test title 1\",\"Text\":\"This is the test of common text\"}",
                "{\"Title\":\"Test title 2\",\"Text\":\"This is the test of numbers {123456789}\"}",
                "{\"Title\":\"Test title 3\",\"Text\":\"This is the test of special {!@#$%^*} characters\"}"
            };
    }
}
