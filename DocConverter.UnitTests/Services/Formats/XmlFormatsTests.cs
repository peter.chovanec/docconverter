﻿using DocConverter.AppLayer.Models;
using DocConverter.AppLayer.Services.Formatters;
using DocConverter.UnitTests.Services.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FluentAssertions;

namespace DocConverter.UnitTests.Services.Formats
{
    public class XmlFormatTests
    {
        [Fact]
        public void Serialize_Should_Return_Correct_XmlString()
        {
            // Arrange
            var documents = DocumentsFixture.GetTestDocs();
            var xmlFormat = new XmlFormat<Document>(false, true);
            var serDocuments = XmlStringsFixture.GetTestDocs(); 

            // Act
            for (int i = 0; i < documents.Count; i++)
            {
                var serDocument = xmlFormat.Serialize(documents[i]);
                Assert.Equal(serDocuments[i], serDocument);
            }
        }

        [Fact]
        public void Deserialize_Should_Return_Correct_Document()
        {
            // Arrange
            var documents = DocumentsFixture.GetTestDocs();
            var xmlFormat = new XmlFormat<Document>();
            var serDocuments = XmlStringsFixture.GetTestDocs();

            // Act
            for (int i = 0; i < documents.Count; i++)
            {
                var document = xmlFormat.Deserialize(serDocuments[i]);
                document.Should().BeEquivalentTo(documents[i]);
            }
        }
    }
}
