﻿using DocConverter.AppLayer.Services.Storages;

namespace DocConverter.UnitTests.Services.Storages
{
    public class FilesystemStorageTests
    {
        [Fact]
        public async Task ReadDocumentAsync_Should_ReturnCorrectContent_When_FileExists()
        {
            // Arrange
            string testFilePath = "testFile.txt";
            string testContent = "This is a test content.";
            File.WriteAllText(testFilePath, testContent);

            var filesystemStorage = new FilesystemStorage(testFilePath);

            // Act
            string result = await filesystemStorage.ReadDocumentAsync();

            // Assert
            Assert.Equal(testContent, result);

            // Clean up
            File.Delete(testFilePath);
        }

        [Fact]
        public async Task ReadDocumentAsync_Should_ThrowFileNotFoundException_When_FileDoesNotExist()
        {
            // Arrange
            string nonExistentFilePath = "nonExistentFile.txt";
            var filesystemStorage = new FilesystemStorage(nonExistentFilePath);

            // Act & Assert
            await Assert.ThrowsAsync<FileNotFoundException>(async () => await filesystemStorage.ReadDocumentAsync());
        }

        [Fact]
        public async Task WriteDocumentAsync_Should_WriteCorrectContent_When_WritingToFile()
        {
            // Arrange
            string testFilePath = "testFile.txt";
            string testContent = "This is a test content.";
            var filesystemStorage = new FilesystemStorage(testFilePath);

            // Act
            await filesystemStorage.WriteDocumentAsync(testContent);

            // Assert
            string writtenContent = File.ReadAllText(testFilePath);
            Assert.Equal(testContent, writtenContent);

            // Clean up
            File.Delete(testFilePath);
        }

        [Fact]
        public async Task WriteDocumentAsync_Should_WriteEmptyFile_When_WritingEmptyContent()
        {
            // Arrange
            string testFilePath = "testFile.txt";
            var filesystemStorage = new FilesystemStorage(testFilePath);

            // Act
            await filesystemStorage.WriteDocumentAsync(string.Empty);

            // Assert
            string writtenContent = File.ReadAllText(testFilePath);
            Assert.Equal(string.Empty, writtenContent);

            // Clean up
            File.Delete(testFilePath);
        }
    }
}
