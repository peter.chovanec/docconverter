using DocConverter.AppLayer.Models;
using DocConverter.AppLayer.Services;
using DocConverter.AppLayer.Services.Formatters;
using DocConverter.AppLayer.Services.Storages;
using DocConverter.UnitTests.Services.Fixtures;
using Microsoft.Extensions.Logging;
using Moq;
using System.Text.Json;

namespace DocConverter.UnitTests.Services
{
    public class DocumentConverterTests
    {
        [Fact]
        public async Task Convert_FileSystem_To_Json_Should_Write_Correct_Json()
        {
            // Arrange
            string xmlDocument = XmlStringsFixture.GetTestDocs()[0];

            var sourceStorage = new Mock<IStorage>();
            sourceStorage.Setup(s => s.ReadDocumentAsync()).ReturnsAsync(xmlDocument);

            var targetStorage = new Mock<IStorage>();
            targetStorage.Setup(s => s.WriteDocumentAsync(It.IsAny<string>())).Returns(Task.CompletedTask);

            var xmlFormat = new XmlFormat<Document>(false, true);
            var jsonFormat = new JsonFormat<Document>(false);

            var loggerMock = new Mock<ILogger<DocumentConverter<Document>>>();

            var converter = new DocumentConverter<Document>(sourceStorage.Object, targetStorage.Object, xmlFormat, jsonFormat, loggerMock.Object);

            // Act
            await converter.ConvertAsync();

            // Assert
            targetStorage.Verify(s => s.WriteDocumentAsync(JsonStringsFixture.GetTestDocs()[0]), Times.Once);
        }

        [Fact]
        public async Task Convert_FileSystem_To_Xml_Should_Write_Correct_Xml()
        {
            // Arrange
            string xmlDocument = JsonStringsFixture.GetTestDocs()[0];

            var sourceStorage = new Mock<IStorage>();
            sourceStorage.Setup(s => s.ReadDocumentAsync()).ReturnsAsync(xmlDocument);

            var targetStorage = new Mock<IStorage>();
            targetStorage.Setup(s => s.WriteDocumentAsync(It.IsAny<string>())).Returns(Task.CompletedTask);

            var xmlFormat = new XmlFormat<Document>(false, true);
            var jsonFormat = new JsonFormat<Document>(false);

            var loggerMock = new Mock<ILogger<DocumentConverter<Document>>>();

            var converter = new DocumentConverter<Document>(sourceStorage.Object, targetStorage.Object, jsonFormat, xmlFormat, loggerMock.Object);

            // Act
            await converter.ConvertAsync();

            // Assert
            targetStorage.Verify(s => s.WriteDocumentAsync(XmlStringsFixture.GetTestDocs()[0]), Times.Once);
        }

    }
}