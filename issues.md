# Issues of the Origin Solution

## God Class

The complete code, including the model, import, conversion and export, is located in one file. It is bad idea because it is difficult to test, modify and extend. Besides of that, It conflicts with SOLID principles, mainly SRP (Single
Responsible Principle).

**Sources:**

[Solid Principles(https://www.medium.com)](https://medium.com/backticks-tildes/the-s-o-l-i-d-principles-in-pictures-b34ce2f1e898)

[Solid Principles(https://www.freecodecamp.org)](https://www.freecodecamp.org/news/solid-principles-explained-in-plain-english/)

## Ambiguous reference

The error *"Ambiguous reference"* occurs because the identifier ``File.Open`` is presented in multiple namespaces, namely ``System.IO`` and ``System.Net.WebRequestMethods``. 

![Ambiguous reference](./Issues/Ambiguous reference.png)

## Variable used out of its scope

The issue with the variable ``input`` is related to its scope. The input variable is defined inside the try-catch block, and it is used later specifically when it is parsed by the XDocument. However, since 
it is declared inside the try-catch block, its scope is limited to that block only.

![Bad declaration](./Issues/Bad declaration.png)

## Error Handling 

The try-catch block in the code is catching exceptions, but it's not providing any useful information to the caller. It's generally not recommended to catch exceptions just to re-throw them with no additional information. It would be better to log the error or provide meaningful information to the caller about what went wrong.

![Error Handling](./Issues/Error Handling.png)

## Resource Management

The code doesn't use ``using`` statements to manage resources like streams and readers. Without using statements we can not be sure the resources are disposed of correctly.

![Usings](./Issues/Usings.png)

## Error-Prone File Paths 

The file paths for the source and target files are currently defined using relative paths, which can lead to errors if the working directory changes. 
I would recommend to use absolute paths defined in application config or to determine the file paths as a input parameter of the application.

![File paths](./Issues/File paths.png)

## Validations/Checks

The code doesn't handle the critical situations, e.g. the source file is not found.

![Validation](./Issues/Validation.png)


