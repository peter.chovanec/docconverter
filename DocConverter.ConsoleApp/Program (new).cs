﻿using DocConverter.AppLayer.Models;
using DocConverter.AppLayer.Services;
using DocConverter.AppLayer.Services.Formatters;
using DocConverter.AppLayer.Services.Storages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;


var serviceProvider = new ServiceCollection()
.AddLogging(builder =>
{
    builder.AddConsole();
})
.BuildServiceProvider();

var logger = serviceProvider.GetService<ILogger<DocumentConverter<Document>>>();

IConfiguration configuration = new ConfigurationBuilder()
.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
.Build();


var sourceFileName = Path.Combine(configuration["SourceFilePath"]!, "Note.xml");
var targetFileName = Path.Combine(configuration["TargetFilePath"]!, "Note.json");
    
var sourceStorage = new FilesystemStorage(sourceFileName);
var targetStorage = new FilesystemStorage(targetFileName);
var sourceFormat = new XmlFormat<Document>(); // or new JsonFormat();
var targetFormat = new JsonFormat<Document>(); // or new XmlFormat();

var converter = new DocumentConverter<Document>(sourceStorage, targetStorage, sourceFormat, targetFormat, logger!);
converter.ConvertAsync().GetAwaiter().GetResult();
