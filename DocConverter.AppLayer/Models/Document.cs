﻿using System;
using System.Xml.Serialization;

namespace DocConverter.AppLayer.Models
{
    [XmlRoot("document")]
    public class Document
    {
        [XmlElement("title")]
        public string Title { get; set; } = String.Empty;
        [XmlElement("text")]
        public string Text { get; set; } = String.Empty;

        public Document()
        {

        }
    }
}
