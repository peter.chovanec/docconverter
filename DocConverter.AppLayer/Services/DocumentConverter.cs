﻿using DocConverter.AppLayer.Models;
using DocConverter.AppLayer.Services.Formatters;
using DocConverter.AppLayer.Services.Storages;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace DocConverter.AppLayer.Services
{
    public class DocumentConverter<T>
    {
        private readonly IStorage _sourceStorage;
        private readonly IStorage _targetStorage;
        private readonly IFormat<T> _sourceFormat;
        private readonly IFormat<T> _targetFormat;
        private readonly ILogger<DocumentConverter<T>> _logger;

        public DocumentConverter(IStorage sourceStorage, IStorage targetStorage, IFormat<T> sourceFormat, IFormat<T> targetFormat, ILogger<DocumentConverter<T>> logger)
        {
            _sourceStorage = sourceStorage ?? throw new ArgumentNullException(nameof(sourceStorage));
            _targetStorage = targetStorage ?? throw new ArgumentNullException(nameof(targetStorage));
            _sourceFormat = sourceFormat ?? throw new ArgumentNullException(nameof(sourceFormat));
            _targetFormat = targetFormat ?? throw new ArgumentNullException(nameof(targetFormat));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task ConvertAsync()
        {
            try
            {
                string input = await _sourceStorage.ReadDocumentAsync();
                var doc = _sourceFormat.Deserialize(input);

                var serializedDoc = _targetFormat.Serialize(doc);
                await _targetStorage.WriteDocumentAsync(serializedDoc);

                _logger.LogInformation("Conversion successful.");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error occurred: " + ex.Message);
            }
        }
    }
}
