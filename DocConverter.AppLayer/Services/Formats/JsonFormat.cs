﻿using DocConverter.AppLayer.Models;
using Newtonsoft.Json;

namespace DocConverter.AppLayer.Services.Formatters
{
    public class JsonFormat<T> : IFormat<T> where T : class, new()
    {
        private readonly bool _formatting;

        public JsonFormat(bool formatting = true)
        {
            _formatting = formatting;
        }

        public string Serialize(T obj)
        {
            return JsonConvert.SerializeObject(obj, _formatting ? Formatting.Indented : Formatting.None);
        }

        public T Deserialize(string data)
        {
            return JsonConvert.DeserializeObject<T>(data)!;
        }
    }
}
