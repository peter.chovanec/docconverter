﻿using DocConverter.AppLayer.Models;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DocConverter.AppLayer.Services.Formatters
{
    public class XmlFormat<T> : IFormat<T> where T : class, new()
    {
        // just for testing purpose
        private readonly bool _formatting;
        private readonly bool _omitDeclaration;

        public XmlFormat(bool formatting = true, bool omitDeclaration = false)
        {
            _formatting = formatting;
            _omitDeclaration = omitDeclaration;
        }

        public string Serialize(T obj)
        {
            var serializer = new XmlSerializer(typeof(T));

            var xdoc = new XDocument();
            using (var writer = xdoc.CreateWriter())
            {
                using (var customWriter = XmlWriter.Create(writer))
                {
                    serializer.Serialize(customWriter, obj);
                }
            }

            var result = xdoc.ToString(_formatting ? SaveOptions.None : SaveOptions.DisableFormatting);
            return _omitDeclaration ? Regex.Replace(result, "<(\\w+)[^>]*>", "<$1>") : result; // just for testing purposes
        }

        public T Deserialize(string data)
        {
            var xdoc = XDocument.Parse(data);

            using (var reader = xdoc.Root.CreateReader())
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }

    }
}


