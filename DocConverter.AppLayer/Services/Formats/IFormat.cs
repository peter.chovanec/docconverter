﻿using DocConverter.AppLayer.Models;

namespace DocConverter.AppLayer.Services.Formatters
{
    public interface IFormat<T>
    {
        string Serialize(T obj);

        T Deserialize(string data);
    }
}
