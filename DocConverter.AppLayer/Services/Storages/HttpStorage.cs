﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DocConverter.AppLayer.Services.Storages
{
    public class HttpStorage : IStorage
    {
        private readonly string _url;

        public HttpStorage(string url)
        {
            _url = url ?? throw new ArgumentNullException(nameof(url));
        }

        public async Task<string> ReadDocumentAsync()
        {
            using (var httpClient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await httpClient.GetAsync(_url);
                    response.EnsureSuccessStatusCode();
                    return await response.Content.ReadAsStringAsync();
                }
                catch (HttpRequestException ex)
                {
                    throw new Exception("Error occurred while reading the document from the URL: " + ex.Message);
                }
            }
        }

        public async Task WriteDocumentAsync(string data)
        {
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(data);
                try
                {
                    HttpResponseMessage response = await httpClient.PostAsync(_url, content);
                    response.EnsureSuccessStatusCode();
                }
                catch (HttpRequestException ex)
                {
                    throw new Exception("Error occurred while writing the document to the URL: " + ex.Message);
                }
            }
        }
    }
}