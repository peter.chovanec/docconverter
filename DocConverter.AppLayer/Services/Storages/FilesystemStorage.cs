﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace DocConverter.AppLayer.Services.Storages
{
    public class FilesystemStorage : IStorage
    {
        private readonly string _filePath;

        public FilesystemStorage(string filePath)
        {
            _filePath = filePath ?? throw new ArgumentNullException(nameof(filePath));
        }

        public Task<string> ReadDocumentAsync()
        {
            if (!File.Exists(_filePath))
            {
                throw new FileNotFoundException("Error occurred while reading the document: Source file not found.");
            }

            return File.ReadAllTextAsync(_filePath);
        }

        public Task WriteDocumentAsync(string data)
        {
            return File.WriteAllTextAsync(_filePath, data);
        }
    }
}
