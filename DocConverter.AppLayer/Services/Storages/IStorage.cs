﻿using System.Threading.Tasks;

namespace DocConverter.AppLayer.Services.Storages
{
    public interface IStorage
    {
        Task<string> ReadDocumentAsync();
        Task WriteDocumentAsync(string data);
    }
}
